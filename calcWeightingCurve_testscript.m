close all
clear
clc

f = 10:10:10e4; % frequency distribution
factorFlag = 'level';

H_weighted_C = calcWeightingCurve(f,factorFlag,'C');
H_weighted_A = calcWeightingCurve(f,factorFlag,'A');

semilogx(f,H_weighted_C);
hold on
semilogx(f,H_weighted_A);

legend('H_C', 'H_A');
title('Weighting curves');

