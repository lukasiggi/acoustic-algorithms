clear!
cd("/media/koerthawkins/DataPartition/Programming/Projects/acoustic-algorithms")
include("calcWeightingCurve.jl")
using PyPlot

f = linspace(10,10e4,10e4/10); # frequency distribution
lF = length(f);
factorFlag = "level";

H_weighted_C = calcWeightingCurve(f,factorFlag,"C");
H_weighted_A = calcWeightingCurve(f,factorFlag,"A");

figure(1)
H_C = plot(f, H_weighted_C, label="H_C")
H_A = plot(f, H_weighted_A, label="H_A")
title("Weighting curves")
xscale("log")
legend()
show()