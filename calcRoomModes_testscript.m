close all 
clear 
clc

l = 7;
d = 2;
h = 2.5;

nMultiples = 2;

modes = calcRoomModes(l, d, h, nMultiples, 'axial');

f = 20:10:20000;

figure();
xlim([f(1), f(end)]);
set(gca, 'XScale', 'log')

nDimensions = length(modes(1, :));

colorCode = winter(3);

for iDimension = 1:nDimensions
    for iMultiple = 1:nMultiples
        plotHandle(iMultiple, iDimension) = ...
            line([modes(iMultiple, iDimension) , ...
                  modes(iMultiple, iDimension)], ...
                  ylim                         , ...
                  'color', colorCode(:, iDimension)); %#ok<SAGROW>
          
        if iMultiple == 1
            currLine = findobj('Type', 'line');
            set(currLine, 'HandleVisibility', 'off');
        end
        
    end
end

xlabel('Frequency / Hz');
set(gca, 'XTick', [0, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000]);
set(gca,'YTick',[]);
ylabel('Modes');
legend(plotHandle(1, :), {'Length modes', 'Height modes', 'Depth Modes'});

title(strcat('Room modes for room: length ', ...
             num2str(l), 'm, height ', ...
             num2str(h), 'm, depth ', ...
             num2str(d), 'm'));