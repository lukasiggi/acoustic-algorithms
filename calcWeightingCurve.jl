function calcWeightingCurve(f,factorFlag="level", curveFlag="A",f1=21,
                            f4=12194,f2=108,f3=738)
# calcWeightingCurve  Calculates and returns the acoustical weighting curve 
#                     for a given frequency distribution. A and C weighting
#                     curve is implemented.
#
#    H_weighted = calcWeightingCurve(f,curveFlag) calculates the weighting
#       curve for a given frequency distribution. It returns the weighting  
#       factor for every point of the frequency distribution either as a 
#       level [dB] or linearily. Either A or C weighting is possible.
#
# In:  f          = frequency points
#      factorFlag = determines whether factors are linear or level,
#                   DEFAULT: level
#      curveFlag  = determines A or C curve,
#                   DEFAULT: A
#      f1         = low frequency pole for C curve, DEFAULT: 21 Hz
#      f4         = high frequency pole for C curve, DEFAULT: 12194 Hz
#      f2         = 1st 1st-order high-pass pole of A curve, 
#                   DEFAULT: 108 Hz
#      f3         = 2nd 1st-order high-pass pole of A curve, 
#                   DEFAULT: 738 Hz
#
# Out: H_weighted = weighting curve as vector 
#
# 2018-09-26, Lukas Maier
# Source: "Akustische Messtechnik" script, Dr. Werner Weselak

## Define constants

C1000 = -0.06; # normalization constant for 1 kHz [dB]
p0    = 10e-5; # reference level [-]

if(factorFlag=="linear")
    C1000 = p0 * 10^(C1000/20); # linearize normalization constant
end

if(curveFlag=="A")
    A1000 = -2; # normalization constant for 1 kHz [dB]
    if(factorFlag=="linear")
        A1000 = p0 * 10^(C1000/20); # linearize normalization constant
    end
end

## Calculate weighting curve
# 

# Get number of frequency points
lF = length(f);

# Statically allocate weighting curve
H_weighted = zeros(lF, 1);

# Calculate weighting curve
if(curveFlag=="C")
    for iFrequency=1:1:lF
        H_weighted[iFrequency] = 20 * log10(f4^2 * f[iFrequency]^2   / 
                                           ((f[iFrequency]^2 + f1^2) * 
                                            (f[iFrequency]^2 + f4^2))) 
                                             - C1000;                 
    end
elseif(curveFlag=="A")
    for iFrequency=1:1:lF
        H_weighted[iFrequency] = 20 * log10(f4^2 * 
                                            f[iFrequency]^2   /  
                                           ((f[iFrequency]^2 + f1^2)  * 
                                            (f[iFrequency]^2 + f4^2)) * 
                                            (f[iFrequency]            / 
                                            sqrt(f[iFrequency]^2 + f2^2)) * 
                                            (f[iFrequency] /
                                            sqrt(f[iFrequency]^2 + f3^2)))  
                                            - A1000;                 
    end
end

# Linearize if necessary
if(factorFlag=="linear")
    H_weighted(:) = 10.^(H_weighted(:)/20);
end

return H_weighted;

end