function varargout = calcRoomModes(l, w, h, nMultiples, modeFlag)
% calcRoomModes  Calculates the room modes for a given rectangulary 
%                shaped room.
% Calculates the room modes of a given rectangulary shaped room.
%
% Out: modes      = cell vector containing the room modes
%
% In:  l          = length of room
%      w          = width of room
%      h          = heigth of room
%      nMultiples = number of multiples used in the algorithm
%                   of room modes
%      modeFlag   = either axial, (can't remember the other ones)
%                   DEFAULT: axial
%
% Out: modes      = cell vector containing the room modes
%
% 2018-09-25, Lukas Maier
% ToDo: -

%% Check input

if(~exist('modeFlag', 'var') || isempty(modeFlag))
  modeFlag = 'axial';
end

%% Define constants

c = 343; % speed of sound / Hz

%% Only axial

if strcmp(modeFlag, 'axial')
  % Preallocate
  modes = zeros(nMultiples * 3, 1);
  
  % Axial length modes
  for iL = 1:nMultiples
    modes(find(~modes, 1, 'first')) = (c/2) * sqrt((iL / l)^2);
  end
  
  % Axial width modes
  for iW = 1:nMultiples
    modes(find(~modes, 1, 'first')) = (c/2) * sqrt((iW / w)^2);
  end
  
  % Axial depth modes
  for iH = 1:nMultiples
    modes(find(~modes, 1, 'first')) = (c/2) * sqrt((iH / h)^2);
  end
end


%% Oblique modes

% for iL = 1:nMultiples
%   for iW = 1:nMultiples
%     for iH = 1:nMultiples
%       modes(iL, iW, iH) = C/2 * sqrt((x/lMultiples(iL))^2 + ...
%                                      (y/lMultiples(iW))^2 + ...
%                                      (z/lMultiples(iH))^2);
%     end
%   end
% end

%% Summarize

modes = reshape(modes, nMultiples, []);

varargout{1} = modes;
